# About

Http Remote for wear is the HTTP Request based version of Mac remote. With Mac remote you could hack around the applescript commands using curl but you would still need a mac running as a server somewhere for all HTTP requests to get executed. 

Http Remote is different in the way the controls per page are limited to a single HTTP request and the request is executed using the network on the handheld directly. So no more mac server needed here. 

And it's completely open source. I've shared the project at bitbucket, check: https://bitbucket.org/cappelleh/android-http-remote

Http Remote for Wear is out! Check https://play.google.com/store/apps/details?id=be.hcpl.android.webwalker for google play entry.

# Implementation Details and Project Examples

Check wiki at https://bitbucket.org/cappelleh/android-http-remote/wiki/Home

# Other Resources

## curl for android

http://stackoverflow.com/questions/4952169/using-curl-in-android

## Alternatives

Some apps that somehow could be used to do this are:

* https://play.google.com/store/apps/details?id=com.sourcestream.android.restclient
* https://play.google.com/store/apps/details?id=com.Konquest.Rest.Client
* https://play.google.com/store/apps/details?id=info.cmlubinski.resttest
* https://play.google.com/store/apps/details?id=com.mauthe.crud
* https://play.google.com/store/apps/details?id=com.merlich.restup_free


# Version History

## 0.2.0

* added import/export using android intent system
* fixed some typo's

## 0.1.0

initial release with minimal required functionality