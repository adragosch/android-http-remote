package be.hcpl.android.macremote.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import be.hcpl.android.macremote.common.Constants;
import be.hcpl.android.macremote.model.HttpControl;

import java.util.ArrayList;
import java.util.List;

public class JsonManager {

    private static JsonManager instance;

    private Context ctx;

    private Gson gson = new Gson();

    private SharedPreferences prefs;

    private JsonManager(Context paramContext) {
        this.ctx = paramContext;
        prefs = ctx.getSharedPreferences(FileManager.PREFS_CONTROLS, Context.MODE_PRIVATE);
    }

    public static JsonManager getInstance(Context paramContext) {
        if (instance == null)
            instance = new JsonManager(paramContext);
        return instance;
    }

    /**
     * get the default config (from assets)
     *
     * @return
     */
    public List<HttpControl> getDefaultItems() {
        return (List) gson.fromJson(FileManager.getInstance(this.ctx).readFileFromAssets(FileManager.FILENAME_DEFAULT_CONTROLS), Constants.HTTP_CONTROL_TYPE);
    }

    /**
     * retrieve a list of all items in config
     *
     * @return
     */
    public List<HttpControl> getAllItems() {
        // get these from preferencs instead
        return (List) gson.fromJson(prefs.getString(FileManager.KEY_CONTROLS, ""), Constants.HTTP_CONTROL_TYPE);
    }

    /**
     * update config with all items
     *
     * @param controls
     */
    private void updateAllItems(List<HttpControl> controls){
        prefs.edit().putString(FileManager.KEY_CONTROLS, gson.toJson(controls, Constants.HTTP_CONTROL_TYPE)).commit();
    }

    /**
     * add a single item
     *
     * @param control
     */
    public void addItem(HttpControl control){
        List<HttpControl> controls = getAllItems();
        controls.add(control);
        updateAllItems(controls);
    }

    /**
     * update a given item
     *
     * @param originalItem
     * @param updatedItem
     */
    public void updateItem(HttpControl originalItem, HttpControl updatedItem) {
        // update a single item goes here
        List<HttpControl> controls = getAllItems();
        if( controls != null ) {
            // search the item
            for (HttpControl c : new ArrayList<>(controls)) {
                if (c.getName() != null && c.getName().equals(originalItem.getName())
                        && (c.getUrl() == null || c.getUrl().equals(originalItem.getUrl()))) {
                    controls.remove(c);
                    break;
                }
            }
            // add the new item
            controls.add(updatedItem);
            // and store the list again
            updateAllItems(controls);
        }
    }

    /**
     * delete a single item
     *
     * @param control
     */
    public void deleteItem(HttpControl control) {
        // update a single item goes here
        List<HttpControl> controls = getAllItems();
        if( controls != null ) {
            // search the item
            for (HttpControl c : new ArrayList<>(controls)) {
                if (c.getName() != null && c.getName().equals(control.getName())
                        && (c.getUrl() == null || c.getUrl().equals(control.getUrl()))) {
                    controls.remove(c);
                    break;
                }
            }
            // and store the list again
            updateAllItems(controls);
        }
    }

    /**
     * duplicate a single item
     *
     * @param selectedItem
     */
    public void copyItem(HttpControl selectedItem) {
        List<HttpControl> controls = getAllItems();
        selectedItem.setName(selectedItem.getName() + " Copy");
        controls.add(selectedItem);
        updateAllItems(controls);
    }

    /**
     * complete config with the list of controls, used for importing data
     * @param items
     */
    public void addAllItems(List<HttpControl> items) {
        List<HttpControl> controls = getAllItems();
        if( controls == null )
            controls = new ArrayList<>();
        controls.addAll(items);
        updateAllItems(controls);
    }

    /**
     * helper to parse json text to list of controls
     *
     * @param text
     * @return
     */
    public List<HttpControl> fromJson(String text) {
        return gson.fromJson(text, Constants.HTTP_CONTROL_TYPE);
    }

    /**
     * helper to convert list of controls to json text
     *
     * @param controls
     * @return
     */
    public String toJson(List<HttpControl> controls) {
        return gson.toJson(controls, Constants.HTTP_CONTROL_TYPE);
    }
}