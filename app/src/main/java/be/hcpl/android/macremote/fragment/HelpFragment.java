package be.hcpl.android.macremote.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.macremote.R;


/**
 * Created by hanscappelle on 23/10/14.
 */
public class HelpFragment extends TemplateFragment {

    /**
     * ctor
     *
     * @return
     */
    public static HelpFragment createInstance() {
        return new HelpFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // don't show help in help fragment
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView textAbout = (TextView) view.findViewById(R.id.text_info);
        textAbout.setText(Html.fromHtml(getString(R.string.info_help_apps)));
        Linkify.addLinks(textAbout, Linkify.ALL);

        view.findViewById(R.id.button_libraries).setVisibility(View.GONE);
        view.findViewById(R.id.divider2).setVisibility(View.GONE);
    }

    @Override
    public int getTitleResourceId() {
        return R.string.title_help;
    }

}
