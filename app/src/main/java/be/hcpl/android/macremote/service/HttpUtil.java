package be.hcpl.android.macremote.service;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;
import java.util.Map;

import be.hcpl.android.macremote.model.RequestType;

/**
 * Created by hcpl on 3/04/15.
 */
public class HttpUtil {

    // TODO add some other types here
    public static final String MEDIA_TYPE_JSON = "text/json; charset=utf-8";

    public static final String MEDIA_TYPE_HTML = "text/html; charset=utf-8";

    public static void performHttpRequest(String url, RequestType type, Map<String,String> headers,
                                    String payloadType, String payload, final Callback callback) throws IOException {

        OkHttpClient client = new OkHttpClient();

        // prepare request
        Request.Builder builder = null;
        Request request = null;

        // http post
        if (type == RequestType.POST) {

            // try parsing but default back to json if that failed
            MediaType mediaType;
            try {
                mediaType = MediaType.parse(payloadType);
            }catch(Exception e){
                mediaType = MediaType.parse(MEDIA_TYPE_JSON);
            }

            builder = new Request.Builder()
                    .url(url)
                    .post(RequestBody.create(mediaType, payload != null ? payload : ""));


        }
        // http get
        else {
            builder = new Request.Builder()
                    .url(url);
        }

        // handle all headers here, ex: .header("Authorization", "Client-ID ")
        if (headers != null && !headers.isEmpty())
            for (String key : headers.keySet())
                builder.header(key, headers.get(key));

        request = builder.build();

        //execute the call here, in sync
//        Response response = client.newCall(request).execute();
//        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

//        Headers responseHeaders = response.headers();
//        for (int i = 0; i < responseHeaders.size(); i++) {
//            System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
//        }

        // execute async
        client.newCall(request).enqueue(callback);
    }
}
