package be.hcpl.android.macremote.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.macremote.MainActivity;
import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.adapter.ItemAdapter;
import be.hcpl.android.macremote.common.Constants;
import be.hcpl.android.macremote.data.JsonManager;
import be.hcpl.android.macremote.model.HttpControl;


/**
 * Created by hanscappelle on 23/10/14.
 */
public class AppListFragment extends TemplateFragment {

    private ItemAdapter adapter;

    private JsonManager jsonManager;

    /**
     * ctor
     *
     * @return
     */
    public static AppListFragment createInstance() {
        return new AppListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // needed for action menu
        setHasOptionsMenu(true);

        // get the db manager
        jsonManager = JsonManager.getInstance(getActivity().getApplicationContext());
    }

    @Override
    public boolean startsWithSeparator() {
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_applist, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView listView = (ListView) view.findViewById(R.id.list);
        adapter = new ItemAdapter(getActivity().getApplicationContext(), null);
        listView.setAdapter(adapter);

        // listen for click events
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((MainActivity) getActivity()).switchContent(AppEditFragment.createInstance((HttpControl) adapter.getItem(position)));
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        List<HttpControl> items = jsonManager.getAllItems();

        if (items == null)
            items = new ArrayList<>();

        adapter.getItems().clear();
        adapter.getItems().addAll(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getTitleResourceId() {
        return R.string.title_manage_apps;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.list_apps, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_add:
                createApp();
                return true;
            case R.id.action_help:
                ((MainActivity) getActivity()).switchContent(HelpFragment.createInstance());
                return true;
            case R.id.action_export:
                shareConfig();
                return true;
            case R.id.action_import:
                importConfig();
                return true;
        }
        return false;
    }

    private void importConfig() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.info_import_export)
                .setCancelable(true)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void shareConfig() {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "HTTP Remote for Android Wear Config");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, jsonManager.toJson(jsonManager.getAllItems()));
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.action_export)));
    }


    private void createApp() {
        ((MainActivity) getActivity()).switchContent(AppEditFragment.createInstance());
    }

}
