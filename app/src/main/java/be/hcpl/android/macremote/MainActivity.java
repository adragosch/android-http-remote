package be.hcpl.android.macremote;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.appframework.TemplateMainActivity;
import be.hcpl.android.macremote.common.Constants;
import be.hcpl.android.macremote.fragment.AboutFragment;
import be.hcpl.android.macremote.fragment.AppListFragment;
import be.hcpl.android.macremote.fragment.HelpFragment;
import be.hcpl.android.macremote.fragment.ControlFragment;
import be.hcpl.android.macremote.event.UpdateAvailableControlsEvent;
import be.hcpl.android.macremote.data.JsonManager;
import be.hcpl.android.macremote.model.HttpControl;

/**
 * this main activity is only in place to handle the fragment navigation, no beacon related stuff
 * to be found here
 */
public class MainActivity extends TemplateMainActivity {

    private JsonManager jsonManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // and title
        setTitle(R.string.app_name);
        //getActionBar().setIcon(getResources().getDrawable(R.drawable.ic_logo));
        // set system bar tint done in style using official L preview impl

        // register for events
        ((MainApplication) getApplication()).getBus().register(this);

        // check if we need to init data here
        jsonManager = JsonManager.getInstance(getApplicationContext());
        if (jsonManager.getAllItems() == null) {
            initConfig();
        }

        // check for intent data here
        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSharedConfig(intent); // Handle text being sent
            }
        }
    }

    private void handleSharedConfig(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText == null) {
            Toast.makeText(this, R.string.err_missing_data, Toast.LENGTH_SHORT).show();
        }

        // remove everything before the { character indicating proper formatted text, this was
        // required for use with Google Note for example where the title was in front
        sharedText = sharedText.substring(sharedText.indexOf("[{"));


        // try to import the data here
        try {
            List<HttpControl> controls = jsonManager.fromJson(sharedText);

            if( controls == null || controls.isEmpty() )
                throw new Exception("No or invalid data");

            jsonManager.addAllItems(controls);

            Toast.makeText(this, R.string.info_data_imported, Toast.LENGTH_SHORT).show();
            ((MainApplication)getApplication()).getBus().post(new UpdateAvailableControlsEvent());
        }catch(Exception e){
            Toast.makeText(this, R.string.err_import_failed, Toast.LENGTH_SHORT).show();
        }


    }

    /**
     * helper to initi local app data on first run
     */
    private void initConfig() {

        // init with all defaults we have in app
        jsonManager.addAllItems(jsonManager.getDefaultItems());

        // update apps needs to be triggered for new menu content here
        ((MainApplication) getApplication()).getBus().post(new UpdateAvailableControlsEvent());
    }

    @Override
    protected void onResume() {
        // always show the about by default
        // TODO create some abstract initial fragment method instead in template
        if (mContentFragment == null)
            mContentFragment = AboutFragment.createInstance();
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((MainApplication) getApplication()).getBus().unregister(this);
    }

    @Subscribe
    public void onEvent(UpdateAvailableControlsEvent event) {
        // just need to refresh menu here
        mNavigationDrawerFragment.getMenuAdapter().getData().clear();
        mNavigationDrawerFragment.getMenuAdapter().getData().addAll(getMenuItems());
        mNavigationDrawerFragment.getMenuAdapter().notifyDataSetChanged();
    }

    public List<TemplateFragment> getMenuItems() {

        if (jsonManager == null)
            jsonManager = JsonManager.getInstance(getApplicationContext());

        // construct items
        List<TemplateFragment> fragments = new ArrayList<>();
        if (jsonManager.getAllItems() != null) {
            for (HttpControl item : jsonManager.getAllItems()) {
                fragments.add(ControlFragment.createInstance(item));
            }
        }

        // general info fragments goes here
        fragments.add(AppListFragment.createInstance());
        fragments.add(HelpFragment.createInstance());
        fragments.add(AboutFragment.createInstance());

        return fragments;
    }


}