package be.hcpl.android.macremote.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

import java.util.Map;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.macremote.MainActivity;
import be.hcpl.android.macremote.MainApplication;
import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.data.JsonManager;
import be.hcpl.android.macremote.event.UpdateAvailableControlsEvent;
import be.hcpl.android.macremote.model.HttpControl;
import be.hcpl.android.macremote.model.RequestType;

/**
 * Created by hanscappelle on 23/10/14.
 */
public class AppEditFragment extends TemplateFragment {

    private static final String KEY_ITEM = "key_item";

    // currently selected item
    private HttpControl selectedItem;

    private JsonManager jsonManager;

    // all views
    private EditText editName, editUrl, editPayload, editPayloadType, editHeaders;

    private CheckBox showResponse;

//    private RadioGroup requestType;

    private RadioButton typeGet, typePost;

    /**
     * ctor for editing an existing app
     *
     * @return
     */
    public static AppEditFragment createInstance(HttpControl item) {
        AppEditFragment f = new AppEditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_ITEM, item);
        f.setArguments(bundle);
        return f;
    }

    /**
     * for creating new items
     *
     * @return
     */
    public static AppEditFragment createInstance() {
        return new AppEditFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get arguments here
        if (savedInstanceState != null)
            selectedItem = (HttpControl) savedInstanceState.getSerializable(KEY_ITEM);

        Bundle args = getArguments();
        if (args != null)
            selectedItem = (HttpControl) args.getSerializable(KEY_ITEM);

        // get the db manager
        jsonManager = JsonManager.getInstance(getActivity().getApplicationContext());

        // needed to show action menu itels
        setHasOptionsMenu(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_ITEM, selectedItem);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_app_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // get all view here
        editName = (EditText) view.findViewById(R.id.edit_name);
        editUrl = (EditText) view.findViewById(R.id.edit_url);
        editPayload = (EditText) view.findViewById(R.id.edit_payload);
        editPayloadType = (EditText) view.findViewById(R.id.edit_payload_type);
        editHeaders = (EditText) view.findViewById(R.id.edit_headers);

        showResponse = (CheckBox) view.findViewById(R.id.check_response);

//        requestType = (RadioGroup) view.findViewById(R.id.radio_type_group);
        typeGet = (RadioButton) view.findViewById(R.id.radio_type_get);
        typePost = (RadioButton) view.findViewById(R.id.radio_type_post);
    }

    @Override
    public void onResume() {
        super.onResume();

        // populate all fields here
        if (selectedItem != null) {
            editName.setText(selectedItem.getName());
            editUrl.setText(selectedItem.getUrl());
            editPayload.setText(selectedItem.getPayload());
            editPayloadType.setText(selectedItem.getPayloadType());

            // headers, using guava
            if( selectedItem.getHeaders() != null ) {
                String joinedHeaders = Joiner.on(";").withKeyValueSeparator(":").join(selectedItem.getHeaders());
                editHeaders.setText(joinedHeaders);
            }

            showResponse.setChecked(selectedItem.isShowResponse());

            typeGet.setChecked(selectedItem.getType() == RequestType.GET);
            typePost.setChecked(selectedItem.getType() == RequestType.POST);

            // also update title
            getActivity().setTitle(new StringBuilder(getString(R.string.title_edit_app)).append(" : ").append(selectedItem.getName()));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_app, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_save:
                updateApp();
                return true;
            case R.id.action_delete:
                deleteApp();
                return true;
            case R.id.action_duplicate:
                copyApp();
                return true;
        }
        return false;
    }

    private void copyApp() {
        jsonManager.copyItem(selectedItem);
        // and navigate back to overview
        ((MainActivity) getActivity()).switchContent(AppListFragment.createInstance());
    }

    private void deleteApp() {
        if( selectedItem == null ){
            // we can just navigate back, nothing created anyway
            getActivity().onBackPressed();
        }

        // otherwise confirm first
        else {
            //Ask the user if they want to quit
            new AlertDialog.Builder(getActivity())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    //.setTitle(R.string.action_delete)
                    .setMessage(R.string.confirm_delete)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // deleteItem
                            jsonManager.deleteItem(selectedItem);
                            // post event
                            ((MainApplication)getActivity().getApplication()).getBus().post(new UpdateAvailableControlsEvent());
                            // and return
                            getActivity().onBackPressed();

                        }

                    })
                    .setNegativeButton(R.string.no, null)
                    .show();
        }
    }

    private void updateApp() {

        // input validation
        if( editName.getText().toString().length() < 2 ){
            Toast.makeText(getActivity(), R.string.msg_input_incomplete, Toast.LENGTH_SHORT).show();
            return;
        }

        // create new app if needed
//        boolean newItem = false;
//        if( selectedItem == null ){
            HttpControl newItem = new HttpControl();
//            newItem = true;
//        }

        // save all input here
        newItem.setName(editName.getText().toString());
        newItem.setUrl(editUrl.getText().toString());
        newItem.setPayload(editPayload.getText().toString());
        newItem.setPayloadType(editPayloadType.getText().toString());
        // type defaults to GET if POST wasn't selected
        newItem.setType(typePost.isChecked() ? RequestType.POST : RequestType.GET);
        // show response or not
        newItem.setShowResponse(showResponse.isChecked());

        // use splitter for the unwrapping of the headers...
        if( editHeaders.getText().toString() != null && editHeaders.getText().toString().length() > 0 ) {
            Map<String, String> headers = Splitter.on(';').trimResults().withKeyValueSeparator(":").split(editHeaders.getText().toString());
            newItem.setHeaders(headers);
        }

        if( selectedItem == null ){
            jsonManager.addItem(newItem);
        } else {
            // and update the item
            jsonManager.updateItem(selectedItem, newItem);
        }

        // if this was the current media item we need to update that one so the events are handled
        // correctly when commands enter from the wearable
        // needed for update of menu
        ((MainApplication)getActivity().getApplication()).getBus().post(new UpdateAvailableControlsEvent());

        // and navigate back to overview
        ((MainActivity) getActivity()).switchContent(AppListFragment.createInstance());
    }


    @Override
    public int getTitleResourceId() {
        return R.string.title_edit_app;
    }

}
