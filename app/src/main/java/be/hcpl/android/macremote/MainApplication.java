package be.hcpl.android.macremote;

import android.app.Application;

import com.squareup.otto.Bus;

import be.hcpl.android.macremote.event.MainThreadEventBus;
import be.hcpl.android.macremote.model.HttpControl;

/**
 * holds the single bus reference
 */
public class MainApplication extends Application {

    private Bus bus = new MainThreadEventBus();

    public Bus getBus() {
        return bus;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
