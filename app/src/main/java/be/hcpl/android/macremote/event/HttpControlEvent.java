package be.hcpl.android.macremote.event;

import be.hcpl.android.macremote.model.HttpControl;

/**
 * event triggered when a http control has to be executed
 *
 * Created by hcpl on 3/04/15.
 */
public class HttpControlEvent {

    private HttpControl item;

    public HttpControlEvent(HttpControl item) {
        this.item = item;
    }

    public HttpControl getItem() {
        return item;
    }

    public void setItem(HttpControl item) {
        this.item = item;
    }
}
