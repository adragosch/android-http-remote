package be.hcpl.android.macremote.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.otto.Subscribe;

import java.io.IOException;

import be.hcpl.android.appframework.TemplateFragment;
import be.hcpl.android.macremote.MainApplication;
import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.model.HttpControl;
import be.hcpl.android.macremote.service.HttpUtil;

public class ControlFragment extends TemplateFragment {

    // minimal control views, limited to a single control matching a single request
    private View playView;

    // visualisation of name of current item
    private TextView nameView;

    // the media item we're working on
    private HttpControl currentItem = null;

    // key used for passing the active control item in bundles
    public static final String KEY_ITEM = "control_item";

    /**
     * ctor, requires a media type (app type) to be created
     *
     * @return
     */
    public static ControlFragment createInstance(final HttpControl item) {
        ControlFragment f = new ControlFragment();
        Bundle b = new Bundle();
        b.putSerializable(KEY_ITEM, item);
        f.currentItem = item;
        return f;
    }

    @Override
    public int getTitleResourceId() {
        // never mind this, we use the dynamic title here
        return R.string.title_control;
    }

    @Override
    public String getDynamicTitle() {
        if (currentItem != null)
            return currentItem.getName();
        else
            return "";
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_ITEM, currentItem);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_control, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        playView = view.findViewById(R.id.image_play);
        nameView = (TextView)view.findViewById(R.id.text_name);

        // set listeners
        playView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // trigger the command here using the event bus
                //((MainApplication) getActivity().getApplication()).getBus().post(new HttpControlEvent(currentItem));
                execute(currentItem);
            }
        });
    }

    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);

        // restore the state
        Bundle args = getArguments();
        if (args != null && args.containsKey(KEY_ITEM)) {
            currentItem = (HttpControl) args.getSerializable(KEY_ITEM);
        } else if (paramBundle != null && paramBundle.containsKey(KEY_ITEM))
            currentItem = (HttpControl) paramBundle.getSerializable(KEY_ITEM);

        // update title based on media item
        if (currentItem != null && currentItem.getName() != null)
            getActivity().setTitle(currentItem.getName());
    }

    @Override
    public void onPause() {
        super.onPause();
        // stop listening
        ((MainApplication) getActivity().getApplication()).getBus().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        // resume listening
        ((MainApplication) getActivity().getApplication()).getBus().register(this);

        // hide keyboard here
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        // and set the title on the button
        nameView.setText(currentItem != null ? currentItem.getName() : "_unknown_");
    }

    private void execute(final HttpControl control) {
        try {
            HttpUtil.performHttpRequest(control.getUrl(), control.getType(), control.getHeaders(),
                    control.getPayloadType(), control.getPayload(),
                    new Callback() {
                        @Override
                        public void onFailure(Request request, IOException err) {
                            // failed for some reason
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), getString(R.string.err_msg_failed), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            if (!response.isSuccessful())
                                throw new IOException("Unexpected code " + response);

                            // reply with response if required
                            if (control.isShowResponse()) {
                                final String responseContent = response.body().string();
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), responseContent, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                    }
            );
        } catch (IOException exception) {
            // failed for some reason
            Toast.makeText(getActivity(), getString(R.string.err_msg_failed), Toast.LENGTH_SHORT).show();
        }
    }
}