package be.hcpl.android.macremote.service;

import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import be.hcpl.android.macremote.MainApplication;
import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.common.Constants;
import be.hcpl.android.macremote.data.JsonManager;
import be.hcpl.android.macremote.event.HttpControlEvent;
import be.hcpl.android.macremote.model.HttpControl;
import be.hcpl.android.macremote.model.RequestType;

/**
 * Created by hanscappelle on 18/11/14.
 */
public class ListenerService extends WearableListenerService {

    private static final long MSG_CONNECTION_TIME_OUT_MS = 1000;

    /**
     * nodeId is stored for the reply
     */
    private String nodeId;

    /**
     * local storage manager
     */
    private JsonManager jsonManager;

    /**
     * single gson reference
     */
    private Gson gson = new Gson();

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        nodeId = messageEvent.getSourceNodeId();
        handleIncomingMessage(messageEvent.getPath());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // register for events
        ((MainApplication) getApplication()).getBus().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // unregister for events
        ((MainApplication) getApplication()).getBus().unregister(this);
    }

    /**
     * helper for toast creation for feedback to user on handheld
     *
     * @param message
     */
    private void handleIncomingMessage(String message) {

        // information only
        Log.d(ListenerService.class.getSimpleName(), "Received message: " + message);


        // if no message just ignore
        if (message == null)
            return;

        // check if we need to init data here, this will be our standard response no matter what
        // actual content of the message was (not return statements, this even blocks)
        jsonManager = JsonManager.getInstance(getApplicationContext());

        // if there are no items known here we need to inform the wearable
        List<HttpControl> allControls = jsonManager.getAllItems();
        if (allControls == null || allControls.isEmpty()) {
            reply(Constants.WEAR_COMMAND_MESSAGE + getString(R.string.err_app_not_configured));
            return;
        }

        // send back a complete list of all available apps in this config
        if (message.startsWith(Constants.WEAR_COMMAND_LIST)) {
            // WHOAHAHAHA don't go from string preference to obj and then back to json !!??
            String jsonMessage = jsonManager.toJson(allControls);
            reply(Constants.WEAR_COMMAND_LIST + jsonMessage);
            return;
        }

        // check if there is an app name in the message (optional param)
        if (message != null && message.startsWith(Constants.WEAR_COMMAND_APPCOMMAND)) {

            // get that app name from the control passed
            HttpControl control = gson.fromJson(message.replace(Constants.WEAR_COMMAND_APPCOMMAND, ""), HttpControl.class);
            // and try to load that app config
            if (control != null) {
                // and send command for processing
                execute(control);
            }
            // inform about failure
            else {
                reply(Constants.WEAR_COMMAND_MESSAGE + getString(R.string.err_msg_failed));
            }
        }
    }

    /**
     * helper for feedback on wearable
     *
     * @param message
     */
    private void reply(final String message) {

        // information only
        Log.d(ListenerService.class.getSimpleName(), "Sending message reply: " + message);

        // perform in background (needed for event handling from bus)
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                // send a reply over the same communication channel
                GoogleApiClient client = new GoogleApiClient.Builder(getApplicationContext())
                        .addApi(Wearable.API)
                        .build();
                client.blockingConnect(MSG_CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                Wearable.MessageApi.sendMessage(client, nodeId, message, null);
                client.disconnect();
            }
        });
        thread.start();

    }

    @Subscribe
    public void onControlEvent(HttpControlEvent event) {

        // validate the data from the event
        if (event == null || event.getItem() == null)
            return;

        execute(event.getItem());
    }

    private void execute(final HttpControl control) {
        try {
            HttpUtil.performHttpRequest(control.getUrl(), control.getType(), control.getHeaders(),
                    control.getPayloadType(), control.getPayload(),
                    new Callback() {
                        @Override
                        public void onFailure(Request request, IOException err) {
                            // failed for some reason
                            reply(Constants.WEAR_COMMAND_MESSAGE + getString(R.string.err_msg_failed));
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            if (!response.isSuccessful())
                                throw new IOException("Unexpected code " + response);

                            // reply with response if required
                            if (control.isShowResponse()) {
                                reply(Constants.WEAR_COMMAND_MESSAGE + response.body().string());
                            }
                        }
                    }
            );
        } catch (IOException exception) {
            // failed for some reason
            reply(Constants.WEAR_COMMAND_MESSAGE + getString(R.string.err_msg_failed));
        }
    }

}