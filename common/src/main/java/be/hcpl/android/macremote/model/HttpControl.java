package be.hcpl.android.macremote.model;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by hcpl on 3/04/15.
 */
public class HttpControl implements Serializable {

    private String name, url, payload, payloadType;

    private RequestType type;

    private Map<String, String> headers;

    private boolean showResponse;

    public boolean isShowResponse() {
        return showResponse;
    }

    public void setShowResponse(boolean showResponse) {
        this.showResponse = showResponse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getPayloadType() {
        return payloadType;
    }

    public void setPayloadType(String payloadType) {
        this.payloadType = payloadType;
    }
}
