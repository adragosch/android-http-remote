package be.hcpl.android.macremote.common;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import be.hcpl.android.macremote.model.HttpControl;

/**
 * Created by hanscappelle on 8/12/14.
 */
public class Constants {

    public static final String WEAR_COMMAND_LIST = "/list/";

    public static final String WEAR_COMMAND_MESSAGE = "/message/";

    public static final String WEAR_COMMAND_APPCOMMAND = "/appcommand/";

    /**
     * use this type for control types
     */
    public static final Type HTTP_CONTROL_TYPE = new TypeToken<List<HttpControl>>() {
    }.getType();

}
