package be.hcpl.android.macremote.model;

/**
 * Created by hcpl on 3/04/15.
 */
public enum RequestType {

    POST, GET
}
