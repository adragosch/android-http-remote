package be.hcpl.android.macremote.wear;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import be.hcpl.android.macremote.R;
import be.hcpl.android.macremote.common.Constants;
import be.hcpl.android.macremote.model.HttpControl;


public class WearActivity extends Activity implements MessageApi.MessageListener {

    private static final long CONNECTION_TIME_OUT_MS = 1000;

    /**
     * status and welcome text
     */
    private TextView mTextView;

    /**
     * logging tag
     */
    private static final String TAG = WearActivity.class.getSimpleName();

    private GoogleApiClient client;

    /**
     * ID form the handheld kept as reference for responses?
     */
//    private String nodeId;

    /**
     * the viewstub reference so we can invalidate it for updates
     */
    private View stub;

    /**
     * a collection of all the apps supported by the configuration found on the device. This list is
     * synced all the time.
     */
    //private List<HttpControl> allApplications = new ArrayList<>();
    List<HttpControlGroup> mGroups = null;

    private class HttpControlGroup {
        String name;
        List<WearHttpControl> apps;
    }
    private class WearHttpControl {
        HttpControl command;
        String groupName;
        String icon;
        String label;
        int order;
        String getGroupName(){return groupName;}
        String getIcon(){return icon;}
        String getLabel(){return label;}
        int getOrder(){return order;}
        void setGroupName(String value){groupName = value;}
        void setIcon(String value){icon = value;}
        void setLabel(String value){label = value;}
        void setOrder(int value){order = value;}
    }

    ImageView mButtonPlay;
    ImageView mButtonPlay1;
    ImageView mButtonPlay2;
    ImageView mButtonPlay3;
    ImageView mButtonPlay4;

    /**
     * the current index in the list
     */
    private int currentIndex = -1;

    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stub = findViewById(R.id.watch_view_stub);
//        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
//            @Override
//            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
                mTextView.setText(R.string.app_name);
                // register the listeners
                mButtonPlay = (ImageView) findViewById(R.id.play);
                mButtonPlay1 = (ImageView) findViewById(R.id.play1);
        mButtonPlay2 = (ImageView) findViewById(R.id.play2);
        mButtonPlay3 = (ImageView) findViewById(R.id.play3);
        mButtonPlay4 = (ImageView) findViewById(R.id.play4);

        mButtonPlay.setOnClickListener(onClickListener);
        mButtonPlay1.setOnClickListener(onClickListener1);
        mButtonPlay2.setOnClickListener(onClickListener2);
        mButtonPlay3.setOnClickListener(onClickListener3);
        mButtonPlay4.setOnClickListener(onClickListener4);

        hideBigPlayButton();
        hidePlayButton(0);
        hidePlayButton(1);
        hidePlayButton(2);
        hidePlayButton(3);

                // just use regular touch gestures for scrolling through list for now since below
                // impl ain't working
                mTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateIndex();
                    }
                });

//                 send an empty message to get a list of apps (init sync)
//                sendMessage("");
                // using prefixes for commands now instead
                sendMessage(Constants.WEAR_COMMAND_LIST);

                // first help info
                // TODO only show once? or let the app control if this should be shown based on usage + settings
                Toast.makeText(getApplication(), R.string.info_toggle_app, Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    private void hideBigPlayButton() {
        mButtonPlay.setVisibility(View.INVISIBLE);
        findViewById(R.id.text_play).setVisibility(View.INVISIBLE);
    }
    private void showBigPlayButton() {
        mButtonPlay.setVisibility(View.VISIBLE);
        findViewById(R.id.text_play).setVisibility(View.VISIBLE);
    }
    private void hidePlayButton(int num) {
        if (num == 0) {
            mButtonPlay1.setVisibility(View.INVISIBLE);
            findViewById(R.id.playText1).setVisibility(View.INVISIBLE);
            findViewById(R.id.playIcon1).setVisibility(View.INVISIBLE);
        }
        else if (num == 1) {
            mButtonPlay2.setVisibility(View.INVISIBLE);
            findViewById(R.id.playText2).setVisibility(View.INVISIBLE);
            findViewById(R.id.playIcon2).setVisibility(View.INVISIBLE);
        }
        else if (num == 2) {
            mButtonPlay3.setVisibility(View.INVISIBLE);
            findViewById(R.id.playText3).setVisibility(View.INVISIBLE);
            findViewById(R.id.playIcon3).setVisibility(View.INVISIBLE);
        }
        else if (num == 3) {
            mButtonPlay4.setVisibility(View.INVISIBLE);
            findViewById(R.id.playText4).setVisibility(View.INVISIBLE);
            findViewById(R.id.playIcon4).setVisibility(View.INVISIBLE);
        }
    }
    private void showPlayButton(int num, WearHttpControl app) {
        if (num == 0) {
            mButtonPlay1.setVisibility(View.VISIBLE);
            findViewById(R.id.playText1).setVisibility(View.VISIBLE);
            findViewById(R.id.playIcon1).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.playText1)).setText(app.getLabel());
            if (app.getIcon() != null && app.getIcon().length() > 0) {
                ((TextView) findViewById(R.id.playIcon1)).setText(app.getIcon());
            }
            else {
                ((TextView) findViewById(R.id.playIcon1)).setText("▶");
            }
        }
        else if (num == 1) {
            mButtonPlay2.setVisibility(View.VISIBLE);
            findViewById(R.id.playText2).setVisibility(View.VISIBLE);
            findViewById(R.id.playIcon2).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.playText2)).setText(app.getLabel());
            if (app.getIcon() != null && app.getIcon().length() > 0) {
                ((TextView) findViewById(R.id.playIcon2)).setText(app.getIcon());
            }
            else {
                ((TextView) findViewById(R.id.playIcon2)).setText("▶");
            }
        }
        else if (num == 2) {
            mButtonPlay3.setVisibility(View.VISIBLE);
            findViewById(R.id.playText3).setVisibility(View.VISIBLE);
            findViewById(R.id.playIcon3).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.playText3)).setText(app.getLabel());
            if (app.getIcon() != null && app.getIcon().length() > 0) {
                ((TextView) findViewById(R.id.playIcon3)).setText(app.getIcon());
            }
            else {
                ((TextView) findViewById(R.id.playIcon3)).setText("▶");
            }
        }
        else if (num == 3) {
            mButtonPlay4.setVisibility(View.VISIBLE);
            findViewById(R.id.playText4).setVisibility(View.VISIBLE);
            findViewById(R.id.playIcon4).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.playText4)).setText(app.getLabel());
            if (app.getIcon() != null && app.getIcon().length() > 0) {
                ((TextView) findViewById(R.id.playIcon4)).setText(app.getIcon());
            }
            else {
                ((TextView) findViewById(R.id.playIcon4)).setText("▶");
            }
        }
    }

    private void setApps(List<WearHttpControl> apps) {
        if (apps.size() == 1) {
            showBigPlayButton();
            hidePlayButton(0);
            hidePlayButton(1);
            hidePlayButton(2);
            hidePlayButton(3);
        }
        if (apps.size() == 2) {
            hideBigPlayButton();
            showPlayButton(0, apps.get(0));
            showPlayButton(1, apps.get(1));
            hidePlayButton(2);
            hidePlayButton(3);
        }
        if (apps.size() == 3) {
            hideBigPlayButton();
            showPlayButton(0, apps.get(0));
            showPlayButton(1, apps.get(1));
            showPlayButton(2, apps.get(2));
            hidePlayButton(3);
        }
        if (apps.size() == 4) {
            hideBigPlayButton();
            showPlayButton(0, apps.get(0));
            showPlayButton(1, apps.get(1));
            showPlayButton(2, apps.get(2));
            showPlayButton(3, apps.get(3));
        }
    }

    private void updateIndex(int desiredIndex) {

        // TODO rewrite this logic

        // go to next item in list
        if (mGroups == null || mGroups.isEmpty())
            return;
        // if we passed a desiredIndex we need to navigate to that position
        if (desiredIndex != Integer.MIN_VALUE) {
            if (desiredIndex >= mGroups.size()) {
                Log.d(TAG, "index too high, move to first item instead");
                desiredIndex = 0;
            }
            currentIndex = desiredIndex;
            HttpControlGroup group = mGroups.get(currentIndex);
            String name = group.name;
            mTextView.setText(name);
            setApps(group.apps);
        }
        // if no desired index passed just go to the next item
        else {
            // reset first if needed
            if (currentIndex >= mGroups.size() - 1)
                currentIndex = -1;
            HttpControlGroup group = mGroups.get(++currentIndex);
            String name = group.name;
            mTextView.setText(name);
            setApps(group.apps);
        }
    }

    private void updateIndex() {
        updateIndex(Integer.MIN_VALUE);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            // check if there is an app to prefix with
            if (currentIndex >= 0 && mGroups != null && currentIndex <= mGroups.size()) {

                HttpControl command = mGroups.get(currentIndex).apps.get(0).command;

                sendMessage(Constants.WEAR_COMMAND_APPCOMMAND + gson.toJson(command));
            }
        }
    };
    private View.OnClickListener onClickListener1 = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            // check if there is an app to prefix with
            if (currentIndex >= 0 && mGroups != null && currentIndex <= mGroups.size()) {

                HttpControl command = mGroups.get(currentIndex).apps.get(0).command;

                sendMessage(Constants.WEAR_COMMAND_APPCOMMAND + gson.toJson(command));
            }
        }
    };
    private View.OnClickListener onClickListener2 = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            // check if there is an app to prefix with
            if (currentIndex >= 0 && mGroups != null && currentIndex <= mGroups.size()) {

                HttpControl command = mGroups.get(currentIndex).apps.get(1).command;

                sendMessage(Constants.WEAR_COMMAND_APPCOMMAND + gson.toJson(command));
            }
        }
    };
    private View.OnClickListener onClickListener3 = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            // check if there is an app to prefix with
            if (currentIndex >= 0 && mGroups != null && currentIndex <= mGroups.size()) {

                HttpControl command = mGroups.get(currentIndex).apps.get(2).command;

                sendMessage(Constants.WEAR_COMMAND_APPCOMMAND + gson.toJson(command));
            }
        }
    };
    private View.OnClickListener onClickListener4 = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            // check if there is an app to prefix with
            if (currentIndex >= 0 && mGroups != null && currentIndex <= mGroups.size()) {

                HttpControl command = mGroups.get(currentIndex).apps.get(3).command;

                sendMessage(Constants.WEAR_COMMAND_APPCOMMAND + gson.toJson(command));
            }
        }
    };

    private GoogleApiClient getGoogleApiClient(Context context) {
        if (client == null)
            client = new GoogleApiClient.Builder(context)
                    .addApi(Wearable.API)
                    .build();
        return client;
    }

    private void sendMessage(final String message) {
        final GoogleApiClient client = getGoogleApiClient(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result =
                        Wearable.NodeApi.getConnectedNodes(client).await();
                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    final String nodeId = nodes.get(0).getId();
                    if (nodeId != null) {

                        client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                        Wearable.MessageApi.sendMessage(client, nodeId, message, null);
                        client.disconnect();

                    }
                    client.disconnect();
                }
            }
        }).start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Wearable.MessageApi.addListener(getGoogleApiClient(getApplicationContext()), this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Wearable.MessageApi.removeListener(getGoogleApiClient(getApplicationContext()), this);
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        // only needed for reply
//        nodeId = messageEvent.getSourceNodeId();
        final String message = messageEvent.getPath();
        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                System.out.println("Message="+message);
                // if the message starts with the magic [list] keyword this is a listing of all
                // available apps known in config
                if (message != null && message.startsWith(Constants.WEAR_COMMAND_LIST)) {

                    // use json here for unwrapping the message instead of index based
//                    allApplications = message.replace(Constants.WEAR_COMMAND_LIST, "").split(";");
                    List<HttpControl> allApplications = (List) gson.fromJson(message.replace(Constants.WEAR_COMMAND_LIST, ""), Constants.HTTP_CONTROL_TYPE);
                    //Names:
                    // {'groupName':'Sonos Wohnzimmer';'icon':'||';'label':'stop';'order':2}

                    HashMap<String, HttpControlGroup> group2List = new HashMap<String, HttpControlGroup>();
                    mGroups = new ArrayList<HttpControlGroup>();

                    String groupName;
                    HttpControlGroup group;
                    for (HttpControl application: allApplications) {
                        String name = application.getName();
                        try {
                            WearHttpControl app = gson.fromJson(name, WearHttpControl.class);
                            app.command = application;
                            groupName = app.getGroupName();

                            if (group2List.containsKey(groupName)) {
                                group = group2List.get(groupName);
                            }
                            else {
                                group = new HttpControlGroup();
                                group.name = groupName;
                                group.apps = new ArrayList<WearHttpControl>();
                                group2List.put(groupName, group);
                                mGroups.add(group);
                            }
                            group.apps.add(app);
                        }
                        catch (com.google.gson.JsonParseException e) {
                            groupName = name;
                            group = new HttpControlGroup();
                            group.name = groupName;
                            group.apps = new ArrayList<WearHttpControl>();
                            WearHttpControl app = new WearHttpControl();
                            app.setGroupName(groupName);
                            app.setLabel(groupName);
                            app.command = application;
                            app.label = name;
                            group.apps.add(app);
                            mGroups.add(group);
                        }
                        /*
                        int posColon = name.indexOf(":");
                        if (posColon != -1) {
                            groupName = name.substring(0, posColon);
                            if (group2List.containsKey(groupName)) {
                                group = group2List.get(groupName);
                            } else {
                                group = new HttpControlGroup();
                                group.name = groupName;
                                group.apps = new ArrayList<HttpControl>();
                                group2List.put(groupName, group);
                                mGroups.add(group);
                            }
                        }
                        else {
                            groupName = name;
                            group = new HttpControlGroup();
                            group.name = groupName;
                            group.apps = new ArrayList<HttpControl>();
                        }
*/
                    }

                    Comparator<WearHttpControl> comp = new Comparator<WearHttpControl>() {
                        @Override
                        public int compare(WearHttpControl a, WearHttpControl b) {
                            return a.order - b.order;
                        }
                    };
                    for (HttpControlGroup g: mGroups) {
                        Collections.sort(g.apps, comp);
                    }

                    // and reset current index to first item (will be incremented)
                    updateIndex(0);
                }
                // otherwise it's just the name of the current app
//                else if (message != null && message.startsWith(Constants.WEAR_COMMAND_APPNAME)) {
//                    mTextView.setText(message.replace(Constants.WEAR_COMMAND_APPNAME, ""));
                    // and force an update of the screen
//                    stub.invalidate();
//                }
                // or a toast message
                else if (message != null && message.startsWith(Constants.WEAR_COMMAND_MESSAGE)) {
                    Toast.makeText(getApplication(), message.replace(Constants.WEAR_COMMAND_MESSAGE, ""), Toast.LENGTH_SHORT).show();
                }
                // no proper command definition, use logging instead
                else {
                    Log.d(TAG, "received bad prefixed message: " + message);
                }
            }

        });
    }
}
